const fs = require("fs");
const path = require("path");
function problem1(directory, numberOfFiles, cb) {
  if (typeof cb === "function") {
    let arrayOfErrors = [];
    let arrayOfFiles = [];
    let deleteCount = 0;
    let countForFs = 0;
    fs.mkdir(directory, (err) => {
      countForFs += 1;
      if (err) {
        cb(Error("Directory already present"));
      } else {
        for (let index = 0; index < numberOfFiles; index++) {
          let filePath = path.join(
            __dirname + "/" + directory + "/file" + `${index}.json`
          );
          fs.writeFile(filePath, "Pranjal", (err) => {
            countForFs += 1;
            if (err) {
              arrayOfErrors.push(err.message);
            } else {
              if (arrayOfFiles.length === numberOfFiles) {
              } else {
                arrayOfFiles.push(filePath);
              }
              if (
                arrayOfFiles.length === numberOfFiles &&
                countForFs - 1 === numberOfFiles
              ) {
                countForFs = 0;
                for (let index = 0; index < numberOfFiles; index++) {
                    fs.unlink(arrayOfFiles[index], (err) => {
                      countForFs += 1;
                      if (err) {
                        arrayOfErrors.push(err.message);
                      } else {
                        deleteCount += 1;
                        if (countForFs === numberOfFiles) {
                          if (deleteCount === numberOfFiles) {
                            cb(null, "success");
                          } else {
                            cb(arrayOfErrors);
                          }
                        }
                      }
                    });
                }
              } else {
                if (countForFs === numberOfFiles) {
                  if (
                    arrayOfErrors.length !== 0 &&
                    countForFs === numberOfFiles
                  ) {
                    cb(arrayOfErrors);
                  }
                }
              }
            }
          });
        }
      }
    });
  } else {
    console.error("cb is not a function");
  }
}
module.exports = problem1;
