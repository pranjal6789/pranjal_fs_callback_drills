const fs = require("fs");
function problem2(cb) {
  let deletedCount = 0;
  let fsCount = 0;
  fs.readFile("./lipsum.txt", "utf-8", (err, data) => {
    if (err) {
      cb(Error("File not Found"));
    } else {
      let upperCaseFileContent = data.toString().toUpperCase();
      let newFile = "upperCaseFileContent.txt";
      fs.writeFile("upperCaseFileContent.txt", upperCaseFileContent, (err) => {
        if (err) {
          cb(Error("File is not writable"));
        } else {
          fs.writeFile("filenames.txt", newFile, (err) => {
            if (err) {
              cb(Error("File is not writable"));
            } else {
              fs.readFile(
                "upperCaseFileContent.txt",
                "utf-8",
                (err, upperCaseData) => {
                  if (err) {
                    cb(Error("Not the permission to write the file"));
                  } else {
                    let lowerCaseData = upperCaseData.toString().toLowerCase();
                    let splitDataContent = lowerCaseData.split(".").join("\n");
                    let newFileForSplit = "splitData.txt";
                    fs.writeFile("splitData.txt", splitDataContent, (err) => {
                      if (err) {
                        cb(Error("file is not writable"));
                      } else {
                        fs.appendFile(
                          "filenames.txt",
                          "\n" + newFileForSplit,
                          (err) => {
                            if (err) {
                              cb(Error("File is not append"));
                            } else {
                              fs.readFile(
                                "splitData.txt",
                                "utf-8",
                                (err, data) => {
                                  if (err) {
                                    cb(Error("File is not readable"));
                                  } else {
                                    let sentencedDataForSort = data.split("\n");
                                    sentencedDataForSort.sort(
                                      (firstValue, secondValue) =>
                                        firstValue - secondValue
                                    );
                                    let newSentencedDataForSort =
                                      sentencedDataForSort.join("\n");
                                    let newFileForSentencedDataSort =
                                      "sortData.txt";
                                    fs.writeFile(
                                      newFileForSentencedDataSort,
                                      newSentencedDataForSort,
                                      (err) => {
                                        if (err) {
                                          cb(Error("file is not writable"));
                                        } else {
                                          fs.appendFile(
                                            "filenames.txt",
                                            "\n" + newFileForSentencedDataSort,
                                            (err, data) => {
                                              if (err) {
                                                cb(Error("file is not append"));
                                              } else {
                                                fs.readFile(
                                                  "filenames.txt",
                                                  "utf-8",
                                                  (err, data) => {
                                                    if (err) {
                                                      cb(
                                                        Error(
                                                          "Not permission to read"
                                                        )
                                                      );
                                                    } else {
                                                      let fileData =
                                                        data.split("\n");

                                                      for (
                                                        let index = 0;
                                                        index < fileData.length;
                                                        index++
                                                      ) {
                                                        fsCount += 1;
                                                        fs.unlink(
                                                          fileData[index],
                                                          (err) => {
                                                            if (err) {
                                                              cb(
                                                                Error(
                                                                  "file is not present"
                                                                )
                                                              );
                                                            } else {
                                                              deletedCount += 1;
                                                            }
                                                            if (
                                                              fsCount === 3 &&
                                                              deletedCount === 2
                                                            ) {
                                                              cb(
                                                                null,
                                                                "success"
                                                              );
                                                            }
                                                          }
                                                        );
                                                      }
                                                    }
                                                  }
                                                );
                                              }
                                            }
                                          );
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    });
                  }
                }
              );
            }
          });
        }
      });
    }
  });
}
module.exports = problem2;
